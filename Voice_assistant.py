import speech_recognition as sr
import pyjokes
import pyttsx3
import datetime
import wikipedia
import webbrowser
import os
import time
import subprocess
import ecapture as ec
import wolframalpha
import json
import requests

engine=pyttsx3.init('sapi5')
voices=engine.getProperty('voices')
engine.setProperty('voice', voices[1].id)
r=sr.Recognizer()
source=sr.Microphone()

def match(stmt, strings):
    return any(ele in strings for ele in statement)

def speak(text):
    print(text)
    engine.say(text)
    engine.runAndWait()

def wishMe():
    hour=datetime.datetime.now().hour
    if hour>=0 and hour<12:
        speak("Hello, Good Morning")
    elif hour>=12 and hour<18:
        speak("Hello, Good Afternoon")
    else:
        speak("Hello, Good Evening")

def takeCommand():
    with source:
        print("Listening...")
        audio=r.listen(source)

        try:
            statement=r.recognize_google(audio, language='en-in')
            print(f"user said: {statement}\n")
        except sr.UnknownValueError :
            speak("I'm sorry I didn't understand")
        except sr.RequestError as e :
            speak("I'm sorry I didn't understand")
            print("Request Failed; {0}".format(e))
        except Exception as e :
            speak("Pardon me, Please say that again")
        return statement

speak('Loading your AI personal assistant Aurora')
wishMe()

if __name__=='__main__':

    while True:
        speak("Tell me how I can be of help")
        statement = takeCommand().lower()
        if statement==0:
            continue

        if match(statement,["goodbye", "ok bye", "bye", "stop"]):
            speak('Your personal assistant Aurora is shutting down, bye bye')
            break

        if match(statement, ["wikipedia"]):
            speak('Searching Wikipedia...')
            statement=statement.replace("wikipedia", "")
            results=wikipedia.summary(statement, sentences=5)
            speak('According to wikipedia')
            speak(results)

        elif match(statement, ["youtube"]):
            webbrowser.open_new_tab("https://www.youtube.com")
            speak('I have now opened youtube for you')
            time.sleep(5)

        elif match(statement, ["time"]):
            strTime=datetime.datetime.now().strftime("%H:%M:%S")
            speak(f"The time is {strTime}")

        elif match(statement, ["who are you", "what can you do"]):
            speak('I am Aurora, your personal assistant. I am programmed to respond to multiple key words or phrases. Such key words or phrases will then trigger actions.')

        elif match(statement, ["who made you", "who created you"]):
            speak('I was programmed by Mady')

        elif match(statement, ["open stack", "open stack overflow", "stack overflow"]):
            webbrowser.open_new_tab("https://stackoverflow.com")
            speak('Stack overflow has been opened')
        
        elif match(statement, ["log off", "sign out", "close computer"]) :
            speak('Ok, logging off pc in 10 seconds. Please close all applications')
            subprocess.call(["shutdown", "/1"])

        elif match(statement, ["ask"]) :  
            speak("I can answer any geographical or computational question you may have. What would you like to know.")
            question = takeCommand()
            app_id = "5AWEA2-K3EG4A94EA"
            client = wolframalpha.Client(app_id)
            res = client.query(question)
            answer = next(res.results).text
            speak(answer)

        elif match(statement, ["search"]) :
            statement = statement.replace("search", "")
            webbrowser.open_new_tab(statement)
            time.sleep(5)

        elif match(statement, ["news"]) :
            news = webbrowser.open_new_tab("https://www.cbc.ca/news/canada")
            speak("Here are some headlines from CBC. Happy reading!")
            time.sleep(5)

        elif match(statement, ["joke", "tell me something funny", "make me laugh"]) :
            joke = pyjokes.get_joke(language='en', category='all')
            speak(joke)

time.sleep(3)